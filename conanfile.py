from conans import ConanFile, CMake, tools
import shutil

class SentencepieceConan(ConanFile):
    name            = "sentencepiece"
    version         = "0.1.92"
    license         = "Apache-2.0"
    author          = "toge.mail@gmail.com"
    url             = "https://bitbucket.org/toge/conan-sentencepiece/"
    homepage        = "https://github.com/google/sentencepiece"
    description     = "Unsupervised text tokenizer for Neural Network-based text generation. "
    topics          = ("unsupervised", "tokenizer", "Neural Network")
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False], "builtin_protobuf": [True, False]}
    default_options = {"shared": False, "builtin_protobuf": True}
    generators      = "cmake"

    def source(self):
        tools.get("https://github.com/google/sentencepiece/archive/v{}.zip".format(self.version))
        shutil.move("sentencepiece-{}".format(self.version), "sentencepiece")
        tools.replace_in_file("sentencepiece/CMakeLists.txt", "set(CMAKE_CXX_STANDARD_REQUIRED ON)",
                              '''set(CMAKE_CXX_STANDARD_REQUIRED ON)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.definitions["SPM_ENABLE_NFKC_COMPILE"]      = False
        cmake.definitions["SPM_ENABLE_SHARED"]            = self.options.shared
        cmake.definitions["SPM_BUILD_TEST"]               = False
        cmake.definitions["SPM_COVERAGE"]                 = False
        cmake.definitions["SPM_ENABLE_TENSORFLOW_SHARED"] = False
        cmake.definitions["SPM_ENABLE_TCMALLOC"]          = False
        cmake.definitions["SPM_TCMALLOC_STATIC"]          = False
        cmake.definitions["SPM_NO_THREADLOCAL"]           = False
        # TODO: Make protobuf to conan recipe
        cmake.definitions["SPM_USE_BUILTIN_PROTOBUF"]     = self.options.builtin_protobuf

        cmake.configure(source_folder="sentencepiece")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="sentencepiece/src")
        if self.options["builtin_protobuf"]:
            self.copy("*.h", dst="include", src="sentencepiece/third_party/protobuf-lite")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["sentencepiece"]
